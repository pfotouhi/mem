# Akita Memory Model

[![Discord](https://img.shields.io/discord/526419346537447424.svg)](https://discord.gg/dQGWq7H)

[![Go Report Card](https://goreportcard.com/badge/gitlab.com/pfotouhi/mem)](https://goreportcard.com/report/gitlab.com/pfotouhi/mem)
[![Test](https://gitlab.com/pfotouhi/mem/badges/master/pipeline.svg)](https://gitlab.com/pfotouhi/mem/commits/master)
[![Coverage](https://gitlab.com/pfotouhi/mem/badges/master/coverage.svg)](https://gitlab.com/pfotouhi/mem/commits/master)

This repository contains the definition and the implementation for memory
system components.

Please check [here](doc/index.md) For detailed documentation.